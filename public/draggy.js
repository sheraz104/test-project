var dragDrop = require('drag-drop')
var WebTorrent = require('webtorrent')

window.client = new WebTorrent()

// When user drops files on the browser, create a new torrent and start seeding it!
dragDrop('#hero1', function (files) {
  client.seed(files, function (torrent) {
  console.log('Client is seeding ' + torrent.magnetURI);
  document.getElementById('magnetLink').innerText=torrent.magnetURI;

  })
})

